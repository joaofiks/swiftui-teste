//
//  ContentView.swift
//  Weather-App
//
//  Created by Joao Fiks on 04/02/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
            LinearGradient(gradient: Gradient(colors: [.blue,Color("lightBlue")]), startPoint: .topLeading, endPoint: .bottomTrailing)
                .edgesIgnoringSafeArea(.all)
            VStack{
                Text("Teresópolis, RJ")
                    .font(.system(size: 32, weight: .medium))
                    .foregroundColor(.white)
                    .padding()
                
                MainWeatherView(imageName: "cloud.sun.fill", temperature: 17)
                
                Spacer()
                
                HStack(spacing:20){
                    WeatherDayView(day: "TER", imageName: "cloud.sun.fill", temperature: 15)
                    
                    WeatherDayView(day: "QUA", imageName: "sun.max.fill", temperature: 17)
                    
                    WeatherDayView(day: "QUI", imageName: "wind.snow", temperature: 9)
                    
                    WeatherDayView(day: "SEX", imageName: "sun.max.fill", temperature: 15)
                    
                    WeatherDayView(day: "SAB", imageName: "cloud.sun.fill", temperature: 7)
                }
                Spacer()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct WeatherDayView: View {
    
    var day: String
    var imageName: String
    var temperature: Int
    
    var body: some View {
        VStack{
            Text(day)
                .font(.system(size: 16, weight: .medium))
                .foregroundColor(.white)
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
            Text("\(temperature)°")
                .font(.system(size: 28, weight: .medium))
                .foregroundColor(.white)
        }
    }
}

struct MainWeatherView: View {
    
    var imageName: String
    var temperature: Int
    
    var body: some View {
        VStack(spacing: 10){
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 180, height: 180)
            Text("\(temperature)°C")
                .font(.system(size: 70, weight: .medium))
                .foregroundColor(.white)
        }
    }
}
