//
//  Weather_AppApp.swift
//  Weather-App
//
//  Created by Joao Fiks on 04/02/21.
//

import SwiftUI

@main
struct Weather_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
